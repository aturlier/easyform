# README #

### Description de cette zone de dépot ###

* Ce dépot contient notre projet de Licence Professionnelle IEM. Le but du projet est de créer une application permettant aux utilisateurs de créer des formulaires de façon intuitive, et de permettre à d'autre personnes d'y répondre par la suite.
* Version : 0.1.0
* Le projet n'est pas dans un état final. Il peut encore être amélioré. Cette version est la version aboutie de nos deux semaines de travail sur l'application.

### Comment démarrer ###

* Cloner le dépot : ```git clone https://aturlier@bitbucket.org/aturlier/easyform.git```
* Dans le dossier cloné, ouvrir le sous-dossier "easyform" avec Android Studio.
* Lancer le projet sur un téléphone ou un émulateur grâce à l'interface d'Android Studio.

### Comment participer au développement de l'application ###

* Clonez le projet comme indiqué ci-dessus.
* Nous recommandons [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/) pour sa gestion simplifiée et efficace des branches et autres fonctionnalités git.
* Vous pouvez alors rajouter des fonctionnalités tout en suivant la convention git-flow pour une meilleure gestion/organisation de ce dépot.

### Qui contacter ###

* Axel Turlier : axel.turlier@outlook.fr
* Benjamin Saugues : benji.saugues@gmail.com
* Nicolas Carpentier : nico6959.nc@gmail.com

### Modifications prévues ou à prévoir ###

* Ajouter un serveur pour stocker les comptes utilisateurs et les formulaires (ceux créés par l'administrateur, et ceux remplis par les clients)
* Amélioration du code : Réduire la redondance, augmenter la robustesse de l'application en général.
* Externalisation de tout les textes dans un fichier strings.xml pour pouvoir proposer l'application en plusieurs langues.
* Utilisation des services Fabric de Twitter pour pouvoir suivre l'utilisation et les potentiels crashs de l'application. On pourra aussi utiliser Fabric pour déployer rapidement l'application sur une liste de téléphones.
* Amélioration de l'interface utilisateur.
* Créer des releases git accompagnées de tags pour suivre l'avancement de l'application simplement.