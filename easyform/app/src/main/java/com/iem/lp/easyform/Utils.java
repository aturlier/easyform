package com.iem.lp.easyform;

import android.os.Environment;

import com.google.gson.Gson;
import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.Form;
import com.iem.lp.easyform.model.Question;
import com.iem.lp.easyform.model.builder.AnswerBuilder;
import com.iem.lp.easyform.model.builder.ButtonQuestionBuilder;
import com.iem.lp.easyform.model.builder.DropDownQuestionBuilder;
import com.iem.lp.easyform.model.builder.FormBuilder;
import com.iem.lp.easyform.model.builder.PickerQuestionBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by aturlier on 05/01/17.
 */
public class Utils {

    public final static String PICKER_QUESTION_TYPE = "PickerQuestion";
    public final static String PICKER_ANSWER_TYPE = "PickerAnswer";
    public final static String BUTTON_QUESTION_TYPE = "ButtonQuestion";
    public final static String BUTTON_ANSWER_TYPE = "ButtonAnswer";
    public final static String DROPDOWN_QUESTION_TYPE = "DropDownQuestion";
    public final static String DROPDOWN_ANSWER_TYPE = "DropDownAnswer";
    public final static String BUTTON_QUESTION_TYPE_YES_NO = "YesNo";
    public final static String BUTTON_QUESTION_TYPE_GENDER = "Gender";
    public final static String BUTTON_QUESTION_TYPE_CUSTOM = "Custom";


    public static Question getQuestionClassFromType(String type) {
        //TODO : Write real method here
        return new ButtonQuestionBuilder()
                .setType(type)
                .createButtonQuestion();
    }

    public static Answer getAnswerClassFromType(String type) {
        //TODO : Write real method here
        return new AnswerBuilder()
                .setType(type)
                .createAnswer();
    }

    public static JSONObject stringToJSONObject(String jsonString) throws Exception {
        JSONObject jsonObject = new JSONObject(jsonString);
        System.out.println(jsonObject);
        return jsonObject;
    }

    public static JSONArray stringToJSONArray(String jsonString) throws Exception {
        JSONArray jsonArray = new JSONArray(jsonString);
        System.out.println(jsonArray);
        return jsonArray;
    }

    public static void writeFormToFile(Form form) throws Exception {
        File root = Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/Android/data/com.iem.lp.easyform/files/");
        dir.mkdirs();
        File file = new File(dir, form.getId() + ".json");
        if (!file.exists()) {
            if (!file.createNewFile()) {
                throw new Exception("File couldn't be created");
            }
        }
        PrintWriter out = new PrintWriter(file);
        out.print(Utils.formToJSON(form));
        out.close();
    }

    public static void writeFormsToFile(ArrayList<Form> forms) throws Exception {
        for (int i = 0; i < forms.size(); i++) {
            writeFormToFile(forms.get(i));
        }
    }

    public static String readFileToString(String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        fin.close();
        return ret;
    }

    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static Form jsonToForm(JSONObject jsonObject) throws Exception {
        if (jsonObject != null) {
            ArrayList<Question> questions = new ArrayList<>();
            JSONArray questionsJSON = jsonObject.getJSONArray("questions");
            for (int i = 0; i < questionsJSON.length(); i++) {
                ArrayList<Answer> answers = new ArrayList<>();
                JSONObject questionJSON = questionsJSON.getJSONObject(i);
                Answer answerForPicker = new Answer();
                if (!questionJSON.getString("type").equals(Utils.PICKER_QUESTION_TYPE)) {
                    JSONArray answersJSON = questionJSON.getJSONArray("possibleAnswers");
                    for (int j = 0; j < answersJSON.length(); j++) {
                        JSONObject answerJSON = answersJSON.getJSONObject(j);
                        answers.add(new AnswerBuilder()
                                .setId(answerJSON.getString("id"))
                                .setSelected(answerJSON.getBoolean("selected"))
                                .setText(answerJSON.getString("text"))
                                .setType(answerJSON.getString("type"))
                                .createAnswer());
                    }
                } else {
                    answerForPicker = new AnswerBuilder()
                            .setId(questionJSON.getJSONObject("answer").getString("id"))
                            .setSelected(questionJSON.getJSONObject("answer").getBoolean("selected"))
                            .setText(questionJSON.getJSONObject("answer").getString("text"))
                            .setType(questionJSON.getJSONObject("answer").getString("type"))
                            .createAnswer();
                }
                switch (questionJSON.getString("type")) {
                    case Utils.BUTTON_QUESTION_TYPE:
                        questions.add(new ButtonQuestionBuilder()
                                .setId(questionJSON.getString("id"))
                                .setPossibleAnswers(answers)
                                .setType(questionJSON.getString("type"))
                                .setText(questionJSON.getString("text"))
                                .setMaxNumberOfChoices(questionJSON.getInt("maxNumberOfChoices"))
                                .createButtonQuestion());
                        break;
                    case Utils.DROPDOWN_QUESTION_TYPE:
                        questions.add(new DropDownQuestionBuilder()
                                .setId(questionJSON.getString("id"))
                                .setPossibleAnswers(answers)
                                .setType(questionJSON.getString("type"))
                                .setText(questionJSON.getString("text"))
                                .createDropDownQuestion());
                        break;
                    case Utils.PICKER_QUESTION_TYPE:
                        questions.add(new PickerQuestionBuilder()
                                .setId(questionJSON.getString("id"))
                                .setAnswer(answerForPicker)
                                .setPickerMin(questionJSON.getInt("pickerMin"))
                                .setPickerMax(questionJSON.getInt("pickerMax"))
                                .setType(questionJSON.getString("type"))
                                .setText(questionJSON.getString("text"))
                                .createPickerQuestion());
                        break;
                }
            }
            return new FormBuilder()
                    .setDuration(jsonObject.getString("duration"))
                    .setQuestions(questions)
                    .setId(jsonObject.getString("id"))
                    .setName(jsonObject.getString("name"))
                    .createForm();
        }
        return null;
    }

    public static ArrayList<Form> jsonToForms(JSONArray jsonArray) throws Exception {
        ArrayList<Form> form = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            form.add(Utils.jsonToForm(jsonArray.getJSONObject(i)));
        }
        return form;
    }

    public static String formToJSON(Form form) {
        Gson gson = new Gson();
        return gson.toJson(form);
    }

    public static String formToJSON(ArrayList<Form> forms) {
        Gson gson = new Gson();
        return gson.toJson(forms);
    }


    //TODO : Demo method. Delete before prod
    public static Form createDemoForm(int questionCount) {
        ArrayList<Question> questions = new ArrayList<>();
        for (int i = 0; i < questionCount; i++) {
            ArrayList<Answer> answers = new ArrayList<>();
            for (int j = 0; j < 4; j++) {
                answers.add(new AnswerBuilder()
                        .setId(Integer.toString(i) + "-" + Integer.toString(j))
                        .setText(Integer.toString(j))
                        .setType(Utils.DROPDOWN_ANSWER_TYPE)
                        .createAnswer());
            }
            questions.add(new DropDownQuestionBuilder()
                    .setId(Integer.toString(i))
                    .setText("Question n°" + Integer.toString(i))
                    .setType(Utils.DROPDOWN_QUESTION_TYPE)
                    .setPossibleAnswers(answers)
                    .createDropDownQuestion());
        }
        Form form = new FormBuilder()
                .setId(new Date().toString())
                .setName("Welcome to the Demo Form")
                .setDuration("10 min")
                .setQuestions(questions)
                .createForm();
        return form;
    }

}
