package com.iem.lp.easyform.model;

import java.io.Serializable;

/**
 * Created by aturlier on 05/01/17.
 */
public class Answer implements Serializable {

    private String id;
    private String text;
    private String type;
    private boolean selected;

    public Answer(String id, String text, String type, boolean selected) {
        this.id = id;
        this.text = text;
        this.type = type;
        this.selected = selected;
    }

    public Answer() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
