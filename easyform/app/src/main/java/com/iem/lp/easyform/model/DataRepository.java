package com.iem.lp.easyform.model;

import java.util.ArrayList;

/**
 * Created by aturlier on 05/01/17.
 */
public class DataRepository {
    private static DataRepository ourInstance = null;
    private static ArrayList<Form> forms;

    public static DataRepository getInstance() {
        if (ourInstance == null) {
            ourInstance = new DataRepository();
        }
        return ourInstance;
    }

    private DataRepository() {
        forms = new ArrayList<>();
    }

    public static void setOurInstance(DataRepository ourInstance) {
        DataRepository.ourInstance = ourInstance;
    }

    public ArrayList<Form> getForms() {
        return forms;
    }

    public static void setForms(ArrayList<Form> forms) {
        DataRepository.forms = forms;
    }
}
