package com.iem.lp.easyform.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by aturlier on 11/01/2017.
 */

public class DropDownQuestion implements Question, Serializable {

    String id = "";
    String text = "";
    String type = "";
    ArrayList<Answer> possibleAnswers = null;

    public DropDownQuestion(String id, String text, String type, ArrayList<Answer> possibleAnswers) {
        this.id = id;
        this.text = text;
        this.type = type;
        this.possibleAnswers = possibleAnswers;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public ArrayList<Answer> getPossibleAnswers() {
        return possibleAnswers;
    }

    @Override
    public void setPossibleAnswers(ArrayList<Answer> possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
    }
}
