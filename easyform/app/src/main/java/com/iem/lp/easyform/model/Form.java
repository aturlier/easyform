package com.iem.lp.easyform.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Benjisora on 05/01/2017.
 */

public class Form implements Serializable{

    private String id;
    private String name;
    private String duration;
    private boolean visible;
    private ArrayList<Question> questions;

    public Form(String id, String name, String duration, ArrayList<Question> questions, boolean visible) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.questions = questions;
        this.visible = visible;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public int getQuestionsCount() {
        return questions.size();
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
