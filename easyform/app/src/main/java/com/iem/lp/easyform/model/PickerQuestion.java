package com.iem.lp.easyform.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by aturlier on 11/01/2017.
 */

public class PickerQuestion implements Question, Serializable {

    String id = "";
    String text = "";
    String type = "";
    Answer answer = null;
    int pickerMin = 0;
    int pickerMax = 0;

    public PickerQuestion(String id, String text, String type, Answer answer, int pickerMin, int pickerMax) {
        this.id = id;
        this.text = text;
        this.type = type;
        answer.setSelected(true);
        answer.setText(Integer.toString(pickerMin));
        this.answer = answer;
        this.pickerMin = pickerMin;
        this.pickerMax = pickerMax;
    }

    public int getPickerMin() {
        return pickerMin;
    }

    public void setPickerMin(int pickerMin) {
        this.pickerMin = pickerMin;
    }

    public int getPickerMax() {
        return pickerMax;
    }

    public void setPickerMax(int pickerMax) {
        this.pickerMax = pickerMax;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public ArrayList<Answer> getPossibleAnswers() {
        ArrayList<Answer> answers = new ArrayList<>();
        answers.add(answer);
        return answers;
    }

    public Answer getAnswer() {
        return answer;
    }

    @Override
    public void setPossibleAnswers(ArrayList<Answer> possibleAnswers) {}
}
