package com.iem.lp.easyform.model;

import java.util.ArrayList;

/**
 * Created by aturlier on 05/01/17.
 */
public interface Question {

    public String getId();

    public void setId(String id);

    public String getText();

    public void setText(String text);

    public String getType();

    public void setType(String type);

    public ArrayList<Answer> getPossibleAnswers();

    public void setPossibleAnswers(ArrayList<Answer> possibleAnswers);
}
