package com.iem.lp.easyform.model.builder;

import com.iem.lp.easyform.model.Answer;

public class AnswerBuilder {
    private String id;
    private String text;
    private String type;
    private boolean selected = false;

    public AnswerBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public AnswerBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public AnswerBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public AnswerBuilder setSelected(boolean selected) {
        this.selected = selected;
        return this;
    }

    public Answer createAnswer() {
        return new Answer(id, text, type, selected);
    }
}