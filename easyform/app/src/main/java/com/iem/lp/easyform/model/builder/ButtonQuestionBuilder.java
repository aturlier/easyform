package com.iem.lp.easyform.model.builder;

import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.ButtonQuestion;

import java.util.ArrayList;

public class ButtonQuestionBuilder {
    private String id;
    private String text;
    private String type = Utils.BUTTON_QUESTION_TYPE;
    private ArrayList<Answer> possibleAnswers;
    private int maxNumberOfChoices = 3;

    public ButtonQuestionBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public ButtonQuestionBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public ButtonQuestionBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public ButtonQuestionBuilder setPossibleAnswers(ArrayList<Answer> possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
        return this;
    }

    public ButtonQuestionBuilder setMaxNumberOfChoices(int maxNumberOfChoices) {
        this.maxNumberOfChoices = maxNumberOfChoices;
        return this;
    }

    public ButtonQuestion createButtonQuestion() {
        return new ButtonQuestion(id, text, type, possibleAnswers, maxNumberOfChoices);
    }
}