package com.iem.lp.easyform.model.builder;

import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.DropDownQuestion;

import java.util.ArrayList;

public class DropDownQuestionBuilder {
    private String id;
    private String text;
    private String type = Utils.DROPDOWN_QUESTION_TYPE;
    private ArrayList<Answer> possibleAnswers;

    public DropDownQuestionBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public DropDownQuestionBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public DropDownQuestionBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public DropDownQuestionBuilder setPossibleAnswers(ArrayList<Answer> possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
        return this;
    }

    public DropDownQuestion createDropDownQuestion() {
        return new DropDownQuestion(id, text, type, possibleAnswers);
    }
}