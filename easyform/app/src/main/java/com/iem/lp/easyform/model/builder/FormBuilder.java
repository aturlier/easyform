package com.iem.lp.easyform.model.builder;

import com.iem.lp.easyform.model.Form;
import com.iem.lp.easyform.model.Question;

import java.util.ArrayList;

public class FormBuilder {
    private String id;
    private String name;
    private String duration = "";
    private ArrayList<Question> questions = null;
    private boolean visible = true;

    public FormBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public FormBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public FormBuilder setDuration(String duration) {
        this.duration = duration;
        return this;
    }

    public FormBuilder setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
        return this;
    }

    public FormBuilder setVisible(boolean visible) {
        this.visible = visible;
        return this;
    }

    public Form createForm() {
        return new Form(id, name, duration, questions, visible);
    }
}