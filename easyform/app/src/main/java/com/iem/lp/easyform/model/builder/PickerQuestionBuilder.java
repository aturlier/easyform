package com.iem.lp.easyform.model.builder;

import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.PickerQuestion;

public class PickerQuestionBuilder {
    private String id;
    private String text;
    private String type = Utils.PICKER_QUESTION_TYPE;
    private Answer answer;
    private int pickerMin = 0;
    private int pickerMax = 100;

    public PickerQuestionBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public PickerQuestionBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public PickerQuestionBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public PickerQuestionBuilder setAnswer(Answer answer) {
        this.answer = answer;
        return this;
    }

    public PickerQuestionBuilder setPickerMin(int pickerMin) {
        this.pickerMin = pickerMin;
        return this;
    }

    public PickerQuestionBuilder setPickerMax(int pickerMax) {
        this.pickerMax = pickerMax;
        return this;
    }

    public PickerQuestion createPickerQuestion() {
        return new PickerQuestion(id, text, type, answer, pickerMin, pickerMax);
    }
}