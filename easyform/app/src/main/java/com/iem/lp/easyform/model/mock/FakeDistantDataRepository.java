package com.iem.lp.easyform.model.mock;

import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.Form;
import com.iem.lp.easyform.model.Question;

import java.util.ArrayList;

/**
 * Created by aturlier on 09/01/17.
 */
public class FakeDistantDataRepository {

    private static FakeDistantDataRepository ourInstance = null;
    private static ArrayList<Form> forms;

    public static FakeDistantDataRepository getInstance() {
        if (ourInstance == null) {
            ourInstance = new FakeDistantDataRepository();
        }
        return ourInstance;

    }

    private FakeDistantDataRepository() {
        forms = new ArrayList<>();
    }

    public static ArrayList<Form> getForms() {
        return forms;
    }

    public static void setForms(ArrayList<Form> forms) {
        FakeDistantDataRepository.forms = forms;
    }

    public void addForm(Form form) {
        if (isFormValid(form)) {
            forms.add(form);
        }
    }

    private boolean isFormValid(Form form) {
        for (Question question : form.getQuestions()) {
            for (Answer answer : question.getPossibleAnswers()) {
                if (answer.isSelected()) {
                    return true;
                }
            }
        }
        return false;
    }
}
