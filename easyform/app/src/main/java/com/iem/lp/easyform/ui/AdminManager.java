package com.iem.lp.easyform.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.DataRepository;
import com.iem.lp.easyform.model.Form;
import com.iem.lp.easyform.model.PickerQuestion;
import com.iem.lp.easyform.ui.adapters.AdminFormListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdminManager extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.admin_manager_form_list)
    ExpandableListView formList;

    @BindView(R.id.admin_manager_logout)
    ImageView logout;

    AdminFormListAdapter adapter;
    ArrayList<Form> forms;

    private class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            //String url = arg1.getExtras().getString("url");
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_manager);
        ButterKnife.bind(this);

        IntentFilter filter = new IntentFilter("com.iem.lp.easyform.NOTIFYFORM");
        this.registerReceiver(new Receiver(), filter);

        forms = DataRepository.getInstance().getForms();
        adapter = new AdminFormListAdapter(this, forms, this);
        formList.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        ArrayList<Boolean> expandedVerificator = new ArrayList<>();
        for(int i = 0; i < adapter.getForms().size(); i++) {
            expandedVerificator.add(formList.isGroupExpanded(i));
        }
        int indexToRemove = forms.indexOf(v.getTag());
        forms.remove(indexToRemove);
        expandedVerificator.remove(indexToRemove);
        adapter = new AdminFormListAdapter(this, forms, this);
        formList.setAdapter(adapter);
        for(int i = 0; i < adapter.getForms().size(); i++) {
            if (expandedVerificator.get(i)) {
                formList.expandGroup(i);
            }
        }
    }

    @OnClick(R.id.admin_manager_fab)
    public void onFabClick() {
        Intent intent = new Intent(AdminManager.this, FormMaker.class);
        startActivity(intent);
    }

    @OnClick(R.id.admin_manager_logout)
    public void onButtonClick() {
        Intent intent = new Intent(AdminManager.this, MainMenuActivity.class);
        startActivity(intent);
        this.finish();
    }
}
