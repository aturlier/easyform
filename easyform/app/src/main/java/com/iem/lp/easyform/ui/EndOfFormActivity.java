package com.iem.lp.easyform.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Form;

public class EndOfFormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_of_form);
        Handler handler = new Handler();

        try {
            Utils.writeFormToFile((Form)getIntent().getExtras().get("FORM"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                EndOfFormActivity.this.finish();
            }
        }, 3000);
    }
}
