package com.iem.lp.easyform.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.DataRepository;
import com.iem.lp.easyform.model.Form;
import com.iem.lp.easyform.model.PickerQuestion;
import com.iem.lp.easyform.model.Question;
import com.iem.lp.easyform.model.builder.AnswerBuilder;
import com.iem.lp.easyform.ui.view.custom.QuestionCreationView;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormMaker extends AppCompatActivity {

    Form formToCreate;

    @BindView(R.id.form_maker_question_creation)
    QuestionCreationView questionCreationView;

    @BindView(R.id.form_maker_form_title)
    EditText formTitle;

    @BindView(R.id.form_maker_back)
    Button back;

    int progress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_maker);
        ButterKnife.bind(this);
        questionCreationView.setQuestion(new PickerQuestion(new Date().toString(), "", Utils.PICKER_QUESTION_TYPE, new AnswerBuilder().setType(Utils.PICKER_ANSWER_TYPE).createAnswer(), 0, 0));
        formToCreate = new Form(new Date().toString(), "", "", new ArrayList<Question>(), true);
        back.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.form_maker_create)
    public void createClick() {
        formToCreate.getQuestions().add(questionCreationView.createNewQuestion());
        if (progress == 0) {
            back.setVisibility(View.VISIBLE);
        }
        progress++;
    }

    @OnClick(R.id.form_maker_back)
    public void backClick() {
        progress--;
        if (progress == 0) {
            back.setVisibility(View.INVISIBLE);
        }
        questionCreationView.setQuestion(formToCreate.getQuestions().get(progress));
    }

    @OnClick(R.id.form_maker_fab)
    public void fabClick() {
        formToCreate.getQuestions().add(questionCreationView.createNewQuestion());
        formToCreate.setName(String.valueOf(formTitle.getText()));
        DataRepository.getInstance().getForms().add(formToCreate);
        Utils.formToJSON(formToCreate);
        this.sendBroadcast(new Intent("com.iem.lp.easyform.NOTIFYFORM"));
        this.finish();
    }
}
