package com.iem.lp.easyform.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.iem.lp.easyform.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginSignUpActivity extends AppCompatActivity {

    @BindView(R.id.activity_login_email)
    EditText email;

    @BindView(R.id.activity_login_password)
    EditText password;

    @BindView(R.id.activity_login_confirm_login)
    Button signUpLoginButton;

    @BindView(R.id.activity_login_close)
    ImageView closeActivity;

    boolean firstLaunch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = this.getApplicationContext().getSharedPreferences("com.iem.lp.easyform", Context.MODE_PRIVATE);
        firstLaunch = sharedPreferences.getBoolean("FIRST_LAUNCH", true);
        if(firstLaunch) {
            signUpLoginButton.setText("Sign Up");
            closeActivity.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.activity_login_confirm_login)
    void connectUser() {
        email.setEnabled(false);
        password.setEnabled(false);

        String typedEmail = String.valueOf(email.getText());
        String typedPassword = String.valueOf(password.getText());

        if(firstLaunch) {
            //Envoyer les infos compte au serveur
            //Si requête réussie
            SharedPreferences sharedPreferences = this.getApplicationContext().getSharedPreferences("com.iem.lp.easyform", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("FIRST_LAUNCH", false);
            editor.apply();
        } else {
            //Vérifier les infos compte par rapport au serveur. Récupérer les formulaires

        }

        //Charger les formulaires de l'utilisateur

        Intent intent = new Intent(LoginSignUpActivity.this, AdminManager.class);
        startActivity(intent);
        this.finish();
    }

    @OnClick(R.id.activity_login_close)
    public void closeClicked() {
        this.finish();
    }

}
