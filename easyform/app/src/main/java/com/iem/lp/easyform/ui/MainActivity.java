package com.iem.lp.easyform.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Form;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Form form = Utils.createDemoForm(2);
        try {
            Form form1 = Utils.jsonToForm(Utils.stringToJSONObject(Utils.formToJSON(form)));
            Log.d("test", "oui");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
