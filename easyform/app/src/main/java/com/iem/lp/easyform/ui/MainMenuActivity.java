package com.iem.lp.easyform.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.ui.adapters.MainMenuAdapter;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainMenuActivity extends AppCompatActivity {

    MainMenuAdapter adapter;

    private class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            //String url = arg1.getExtras().getString("url");
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ButterKnife.bind(this);

        adapter = new MainMenuAdapter(this);

        IntentFilter filter = new IntentFilter("com.iem.lp.easyform.NOTIFYFORM");
        this.registerReceiver(new Receiver(), filter);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainMenuActivity.this, QuestionsActivity.class);
                intent.putExtra("POSITION", position);
                startActivity(intent);
            }
        });
    }

    @OnClick(R.id.main_menu_admin_button)
    public void onClick() {
        Intent intent = new Intent(MainMenuActivity.this, LoginSignUpActivity.class);
        startActivity(intent);
    }
}
