package com.iem.lp.easyform.ui;

import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.DataRepository;
import com.iem.lp.easyform.model.Form;
import com.iem.lp.easyform.model.PickerQuestion;
import com.iem.lp.easyform.ui.view.custom.QuestionView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuestionsActivity extends AppCompatActivity {

    @BindView(R.id.questionsactivity_questionView)
    QuestionView questionView;

    @BindView(R.id.questionsactivity_progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.questionsactivity_form_title)
    TextView textView;

    private Form form;
    int progress = 0;


    private class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            //String url = arg1.getExtras().getString("url");
            Answer a = (Answer) arg1.getExtras().get("ANSWER");
            Log.d("ANSWERED", a.getText());
            if (a.getType().equals(Utils.PICKER_ANSWER_TYPE)) {
                ((PickerQuestion) form.getQuestions().get(progress)).getAnswer().setText(a.getText());
            } else {
                form.getQuestions().get(progress).getPossibleAnswers().get(getAnswerIndex(a)).setSelected(!form.getQuestions().get(progress).getPossibleAnswers().get(getAnswerIndex(a)).isSelected());
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        ButterKnife.bind(this);

        IntentFilter filter = new IntentFilter("com.iem.lp.easyform.SENDQUESTION");
        this.registerReceiver(new Receiver(), filter);

        int position = getIntent().getExtras().getInt("POSITION");

        //TODO : get the correct form and store it temporarily
        form = DataRepository.getInstance().getForms().get(position);
        textView.setText(form.getName());
        questionView.setQuestion(form.getQuestions().get(progress));
        progressBar.setMax(form.getQuestions().size()*100);
        progressBar.setProgress((progress+1)*100);
    }

    private int getAnswerIndex(Answer selectedAnswer) {
        if (selectedAnswer != null) {
            for (Answer answerFromArray : form.getQuestions().get(progress).getPossibleAnswers()) {
                if (selectedAnswer.getId().equals(answerFromArray.getId())) {
                    return form.getQuestions().get(progress).getPossibleAnswers().indexOf(answerFromArray);
                }
            }
        }
        return 0;
    }

    @OnClick(R.id.questionsactivity_button_back)
    public void backPressed() {
        if (progress > 0) {
            progress--;
            questionView.setQuestion(form.getQuestions().get(progress));
            this.decreaseProgressSmoothly();
        }
    }

    @OnClick(R.id.questionsactivity_button_next)
    public void nextPressed() {
        if (progress < form.getQuestions().size()-1) {
            progress++;
            questionView.setQuestion(form.getQuestions().get(progress));
            this.increaseProgressSmoothly();
        } else if (progress == form.getQuestions().size()-1){
            Intent intent = new Intent(QuestionsActivity.this, EndOfFormActivity.class);
            /*
            if (!Utils.sendToServer(form)) {
                Utils.saveOnLocalStorage(form);
            }
             */
            intent.putExtra("FORM", form);
            startActivity(intent);
            this.finish();
        }
    }

    public void clearSelectedAnswers() {
        for (int i = 0; i < form.getQuestions().get(progress).getPossibleAnswers().size(); i++) {
            form.getQuestions().get(progress).getPossibleAnswers().get(i).setSelected(false);
        }
    }

    private void increaseProgressSmoothly() {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", progress*100, (progress+1)*100);
        animation.setDuration(500); // 0.5 second
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.start();
    }

    private void decreaseProgressSmoothly() {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", (progress+2)*100, (progress+1)*100);
        animation.setDuration(500); // 0.5 second
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.start();
    }


    //@Override
    //public void onClick(View v) {
    //    Log.d("Debug", "Coucou 2");
    //    clearSelectedAnswers(v);
    //    form.getQuestions().get(progress).getPossibleAnswers().get(getAnswerIndexFromView(v)).setSelected(true);
    //}
}
