package com.iem.lp.easyform.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.DataRepository;
import com.iem.lp.easyform.model.Form;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreen extends AppCompatActivity {

    @BindView(R.id.splashscreen_loading_image)
    ImageView image;

    @BindView(R.id.splashscreen_loading_image_background)
    ImageView imageBackgroud;

    @BindView(R.id.splashscreen_loading_text)
    TextView loadingText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        //TODO: Load the data, update the picture
        //TEMPORARY :
        String json = "{\"duration\":\"\",\"id\":\"Sun Jan 15 00:15:00 GMT+01:00 2017\",\"name\":\"Formulaire d\\u0027exemple 1\",\"questions\": [{\"id\":\"Sun Jan 15 00:15:10 GMT+01:00 2017\",\"maxNumberOfChoices\":1,\"possibleAnswers\": [{\"id\":\"Sun Jan 15 00:15:30 GMT+01:00 2017\",\"selected\":false,\"text\":\"Oui\",\"type\":\"ButtonAnswer\"},{\"id\":\"Sun Jan 15 00:15:30 GMT+01:00 2017\", \"selected\":false,\"text\":\"Non\",\"type\":\"ButtonAnswer\"}],\"text\":\"Aimez-vous notre application ?\",\"type\":\"ButtonQuestion\"},{\"answer\":{\"id\":\"Sun Jan 15 00:16:35 GMT+01:00 2017\",\"selected\":true,\"text\":\"0\",\"type\":\"PickerAnswer\"},\"id\":\"Sun Jan 15 00:16:35 GMT+01:00 2017\",\"pickerMax\":5,\"pickerMin\":0,\"text\":\"Quelle note (sur 5) donneriez-vous à notre application ?\",\"type\":\"PickerQuestion\"},{\"id\":\"Sun Jan 15 00:17:44 GMT+01:00 2017\",\"possibleAnswers\": [{\"id\":\"Sun Jan 15 00:18:30 GMT+01:00 2017\",\"selected\":false,\"text\":\"Des types de questions\",\"type\":\"DropDownAnswer\"},{\"id\":\"Sun Jan 15 00:18:30 GMT+01:00 2017\",\"selected\":false,\"text\":\"Une meilleure stabilité\",\"type\":\"DropDownAnswer\"},{\"id\":\"Sun Jan 15 00:18:30 GMT+01:00 2017\",\"selected\":false,\"text\":\"Un meilleur design\",\"type\":\"DropDownAnswer\"},{\"id\":\"Sun Jan 15 00:18:30 GMT+01:00 2017\",\"selected\":false,\"text\":\"Des fonctionnalités\",\"type\":\"DropDownAnswer\"}],\"text\":\"Que manque-t-il à notre application ?\",\"type\":\"DropDownQuestion\"}],\"visible\":true}";
        Form form = null;
        try {
            form = Utils.jsonToForm(Utils.stringToJSONObject(json));
        } catch (Exception e) {
            e.printStackTrace();
        }
        DataRepository.getInstance().getForms().add(form);

        final ImageView image = (ImageView) findViewById(R.id.splashscreen_loading_image);
        final ArrayList<String> loadingImages = new ArrayList<>();
        for (int i = 0; i <= 100; i += 20) {
            loadingImages.add("ic_droplet_blood_" + i + "_smile");
        }

        final Handler handler = new Handler();
        handler.post(new Runnable() {

            private int index = 0;

            @Override
            public void run() {
                if (index != loadingImages.size()) {
                    Resources resources = getResources();
                    final int resourceId = resources.getIdentifier(loadingImages.get(index), "drawable", getPackageName());
                    Drawable newImage = ContextCompat.getDrawable(SplashScreen.this, resourceId);
                    Animation fadeIn = AnimationUtils.loadAnimation(SplashScreen.this, R.anim.fade_in);
                    imageBackgroud.setImageDrawable(image.getDrawable());
                    image.startAnimation(fadeIn);
                    image.setImageDrawable(newImage);
                    index++;
                    handler.postDelayed(this, 1000);
                } else {
                    if (loadingText.getText().equals(getString(R.string.splashscreen_loading_done))) {
                        SharedPreferences sharedPreferences = SplashScreen.this.getApplicationContext().getSharedPreferences("com.iem.lp.easyform", Context.MODE_PRIVATE);
                        boolean firstLaunch = sharedPreferences.getBoolean("FIRST_LAUNCH", true);
                        Intent intent;
                        if (firstLaunch) {
                            intent = new Intent(SplashScreen.this, LoginSignUpActivity.class);
                        } else {
                            intent = new Intent(SplashScreen.this, MainMenuActivity.class);
                        }
                        startActivity(intent);
                        SplashScreen.this.finish();
                    } else {
                        loadingText.setText(R.string.splashscreen_loading_done);
                        loadingText.startAnimation(AnimationUtils.loadAnimation(SplashScreen.this, R.anim.fade_in));
                        handler.postDelayed(this, 1000);
                    }
                }
            }
        });
    }
}
