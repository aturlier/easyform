package com.iem.lp.easyform.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.model.Form;
import com.iem.lp.easyform.model.Question;

import java.util.ArrayList;

/**
 * Created by aturlier on 10/01/2017.
 */

public class AdminFormListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Form> forms;
    private View.OnClickListener deleteButtonClickListener;

    public AdminFormListAdapter(Context context, ArrayList<Form> forms, View.OnClickListener deleteButtonClickListener) {
        super();
        this.context = context;
        this.forms = forms;
        this.deleteButtonClickListener = deleteButtonClickListener;
    }

    @Override
    public int getGroupCount() {
        return forms.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return forms.get(groupPosition).getQuestions().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return forms.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return forms.get(groupPosition).getQuestions().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Form formToDisplay = (Form) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.admin_form_list_item, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.admin_form_list_item_cardview_form_title);

        textView.setText(formToDisplay.getName());

        final ImageView imgVisibility = (ImageView) convertView.findViewById(R.id.admin_form_list_item_cardview_visibility_button);
        imgVisibility.setImageDrawable(ContextCompat.getDrawable(AdminFormListAdapter.this.context, !((Form) getGroup(groupPosition)).isVisible() ? R.drawable.ic_visibility_off_black : R.drawable.ic_remove_red_eye_black));
        imgVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgVisibility.setImageDrawable(ContextCompat.getDrawable(AdminFormListAdapter.this.context, ((Form) getGroup(groupPosition)).isVisible() ? R.drawable.ic_visibility_off_black : R.drawable.ic_remove_red_eye_black));
                ((Form) getGroup(groupPosition)).setVisible(!((Form) getGroup(groupPosition)).isVisible());
            }
        });

        ImageView imgDelete = (ImageView) convertView.findViewById(R.id.admin_form_list_item_cardview_delete_button);
        imgDelete.setOnClickListener(this.deleteButtonClickListener);

        imgDelete.setTag(formToDisplay);

        Log.d("Adapter", "Form " + groupPosition);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Question questionToDisplay = (Question)getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_question_item, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.list_question_item_question_name);

        textView.setText(questionToDisplay.getText());

        Log.d("Adapter", "Question " + childPosition + " of form " + groupPosition);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public ArrayList<Form> getForms() { return this.forms; }

}
