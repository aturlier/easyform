package com.iem.lp.easyform.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.model.DataRepository;
import com.iem.lp.easyform.model.Form;

import java.util.ArrayList;

/**
 * Created by nicolas on 09/01/2017.
 */

public class MainMenuAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Form> formList;

    public MainMenuAdapter(Context c) {
        mContext = c;
        mInflater=LayoutInflater.from(mContext);
        formList = DataRepository.getInstance().getForms();
    }

    public int getCount() {
        return formList.size();
    }

    public Object getItem(int position) {
        return formList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }



    // create a new ImageView for each item referenced by the Adapter

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView titleView;
        TextView durationView;
        TextView questionCountView;

        LinearLayout view;
        if (convertView == null) {
            view = (LinearLayout) mInflater.inflate(R.layout.mainmenu_item, parent, false);
            // if it's not recycled, initialize some attributes
        } else {
            view = (LinearLayout) convertView;
        }

        if(!formList.get(position).isVisible()){
            while (position < formList.size() && !formList.get(position).isVisible()){
                position ++;
            }
        }
        if(!formList.get(position).isVisible()){
            return null;
        }

        titleView = (TextView) view.findViewById(R.id.item_title);
        durationView = (TextView) view.findViewById(R.id.item_duration);
        questionCountView = (TextView) view.findViewById(R.id.item_question_count);

        titleView.setText(formList.get(position).getName());
        durationView.setText(formList.get(position).getDuration());
        questionCountView.setText(formList.get(position).getQuestionsCount() + " Questions");


        return view;
    }



    // references to our images

}

