package com.iem.lp.easyform.ui.view.custom;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.ButtonQuestion;
import com.iem.lp.easyform.model.DropDownQuestion;
import com.iem.lp.easyform.model.PickerQuestion;
import com.iem.lp.easyform.model.Question;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by aturlier on 12/01/2017.
 */

public class QuestionCreationView extends RelativeLayout {

    @BindView(R.id.question_creation_view_question_text)
    EditText questionTitleEditText;

    @BindView(R.id.question_creation_view_question_type_spinner)
    Spinner questionTypeSpinner;

    @BindView(R.id.question_creation_view_layout_holder)
    LinearLayout holderLayout;

    private Context context;
    private Question question;
    private final int DEFAULT_ANIMATION_TRANSITION = 500;

    final ArrayList<String> typesArray = new ArrayList<>();

    public QuestionCreationView(Context context) {
        super(context);
        init(context);
    }

    public QuestionCreationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public QuestionCreationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        typesArray.add(Utils.BUTTON_QUESTION_TYPE);
        typesArray.add(Utils.DROPDOWN_QUESTION_TYPE);
        typesArray.add(Utils.PICKER_QUESTION_TYPE);
        this.context = context;
        inflate(this.context, R.layout.question_creation_view, this);
        ButterKnife.bind(this);
        final ArrayList<String> typesArray = new ArrayList<>();
        typesArray.add(Utils.BUTTON_QUESTION_TYPE);
        typesArray.add(Utils.DROPDOWN_QUESTION_TYPE);
        typesArray.add(Utils.PICKER_QUESTION_TYPE);
        questionTypeSpinner.setAdapter(new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_dropdown_item, typesArray));
        questionTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (typesArray.get(position)) {
                    case Utils.BUTTON_QUESTION_TYPE :
                        if(question.getType() != Utils.BUTTON_QUESTION_TYPE) {
                            setQuestion(new ButtonQuestion(new Date().toString(), "", Utils.BUTTON_QUESTION_TYPE, new ArrayList<Answer>(), 1));
                        }
                        break;
                    case Utils.DROPDOWN_QUESTION_TYPE :
                        if(question.getType() != Utils.DROPDOWN_QUESTION_TYPE) {
                            setQuestion(new DropDownQuestion(new Date().toString(), "", Utils.DROPDOWN_QUESTION_TYPE, new ArrayList<Answer>()));
                        }
                        break;
                    case Utils.PICKER_QUESTION_TYPE :
                        if(question.getType() != Utils.PICKER_QUESTION_TYPE) {
                            Answer answer = new Answer();
                            answer.setType(Utils.PICKER_ANSWER_TYPE);
                            setQuestion(new PickerQuestion(new Date().toString(), "", Utils.PICKER_QUESTION_TYPE, answer, 0, 100));
                        }
                        break;
                    default :
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setQuestion(Question question) {
        this.fadeToInvisible(DEFAULT_ANIMATION_TRANSITION);
        this.question = question;
        this.updateView();
        this.fadeToVisible(DEFAULT_ANIMATION_TRANSITION);
    }

    private void fadeToInvisible(int duration) {
        AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration(duration);
        animation.setFillAfter(true);
        this.startAnimation(animation);
    }

    private void fadeToVisible(int duration) {
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(duration);
        animation.setFillAfter(true);
        this.startAnimation(animation);
    }

    private void updateView() {
        if (this.question != null && !this.question.getText().isEmpty()) {
            this.questionTitleEditText.setText(this.question.getText());
        } else {
            this.questionTitleEditText.setText("");
        }
        this.questionTypeSpinner.setSelection(typesArray.indexOf(question.getType()));
        this.cleanHolderLayout();
        createAnswersFromQuestion(question);
    }

    private void cleanHolderLayout() {
        holderLayout.removeAllViews();
    }

    private void createAnswersFromQuestion(Question questionToDisplay) {
        switch (question.getType()) {
            case Utils.BUTTON_QUESTION_TYPE:
                addButtonAnswerCreationToLayoutHolder((ButtonQuestion) questionToDisplay);
                break;
            case Utils.PICKER_QUESTION_TYPE:
                addPickerAnswerCreationToLayoutHolder((PickerQuestion) questionToDisplay);
                break;
            case Utils.DROPDOWN_QUESTION_TYPE:
                addDropDownAnswerCreationToLayoutHolder((DropDownQuestion) questionToDisplay);
                break;
            default:
                break;
        }
    }

    private void addButtonAnswerCreationToLayoutHolder(ButtonQuestion questionToDisplay) {
        //HEADER
        LinearLayout linearLayout = new LinearLayout(this.context);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setWeightSum(100);
        Spinner advancedTypeSpinner = new Spinner(this.context);
        EditText selectionnableAnswerCount = new EditText(this.context);
        selectionnableAnswerCount.setHint("Nombre de selections max.");
        if(!(Integer.toString(questionToDisplay.getMaxNumberOfChoices()).isEmpty())) {
            selectionnableAnswerCount.setText(Integer.toString(questionToDisplay.getMaxNumberOfChoices()));
        }
        selectionnableAnswerCount.setInputType(InputType.TYPE_CLASS_NUMBER);

        ArrayList<String> typesArray = new ArrayList<>();
        typesArray.add(Utils.BUTTON_QUESTION_TYPE_CUSTOM);
        typesArray.add(Utils.BUTTON_QUESTION_TYPE_YES_NO);
        typesArray.add(Utils.BUTTON_QUESTION_TYPE_GENDER);
        ArrayAdapter<String> possibleTypesArrayAdapter = new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item, typesArray);
        advancedTypeSpinner.setAdapter(possibleTypesArrayAdapter);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 50f);
        advancedTypeSpinner.setLayoutParams(lp);
        selectionnableAnswerCount.setLayoutParams(lp);
        linearLayout.addView(advancedTypeSpinner);
        linearLayout.addView(selectionnableAnswerCount);

        holderLayout.addView(linearLayout);

        //FOOTER
        ImageView addImageView = new ImageView(this.context);
        addImageView.setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.ic_add_black));
        addImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final LinearLayout linearLayout = new LinearLayout(QuestionCreationView.this.context);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.setWeightSum(100);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 90f);
                EditText answerTitle = new EditText(QuestionCreationView.this.context);
                answerTitle.setLayoutParams(lp);
                answerTitle.setHint("Intitulé de la question");

                lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 10f);
                lp.gravity = Gravity.CENTER;

                ImageView deleteAnswer = new ImageView(QuestionCreationView.this.context);
                deleteAnswer.setImageDrawable(ContextCompat.getDrawable(QuestionCreationView.this.context, R.drawable.ic_close_black));
                deleteAnswer.setLayoutParams(lp);
                deleteAnswer.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holderLayout.removeView(linearLayout);
                    }
                });
                linearLayout.addView(answerTitle);
                linearLayout.addView(deleteAnswer);
                linearLayout.setTag(new Answer());
                holderLayout.addView(linearLayout, holderLayout.getChildCount() - 1);
            }
        });
        holderLayout.addView(addImageView);

        for(Answer answer : questionToDisplay.getPossibleAnswers()) {
            createButtonOrDropDownAnswer(answer);
        }
    }

    private void createButtonOrDropDownAnswer(Answer answerToCreate) {
        final LinearLayout linearLayout = new LinearLayout(QuestionCreationView.this.context);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setWeightSum(100);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 90f);
        EditText answerTitle = new EditText(QuestionCreationView.this.context);
        answerTitle.setLayoutParams(lp);
        answerTitle.setHint("Intitulé de la question");
        if(answerToCreate != null && !answerToCreate.getText().isEmpty()) {
            answerTitle.setText(answerToCreate.getText());
        }

        lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 10f);
        lp.gravity = Gravity.CENTER;

        ImageView deleteAnswer = new ImageView(QuestionCreationView.this.context);
        deleteAnswer.setImageDrawable(ContextCompat.getDrawable(QuestionCreationView.this.context, R.drawable.ic_close_black));
        deleteAnswer.setLayoutParams(lp);
        deleteAnswer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                holderLayout.removeView(linearLayout);
            }
        });
        linearLayout.addView(answerTitle);
        linearLayout.addView(deleteAnswer);
        linearLayout.setTag(new Answer());
        holderLayout.addView(linearLayout, holderLayout.getChildCount() - 1);
    }

    private void addPickerAnswerCreationToLayoutHolder(PickerQuestion questionToDisplay) {
        //HEADER
        LinearLayout linearLayout = new LinearLayout(this.context);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setWeightSum(100);
        EditText minPickerValue = new EditText(this.context);
        EditText maxPickerValue = new EditText(this.context);
        minPickerValue.setHint("Nombre de selections min.");
        maxPickerValue.setHint("Nombre de selections max.");
        minPickerValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        maxPickerValue.setInputType(InputType.TYPE_CLASS_NUMBER);

        if(!(Integer.toString(questionToDisplay.getPickerMin())).isEmpty() && !(Integer.toString(questionToDisplay.getPickerMax())).isEmpty()) {
            minPickerValue.setText((Integer.toString(questionToDisplay.getPickerMin())));
            maxPickerValue.setText((Integer.toString(questionToDisplay.getPickerMax())));
        }

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 50f);
        minPickerValue.setLayoutParams(lp);
        maxPickerValue.setLayoutParams(lp);
        linearLayout.addView(minPickerValue);
        linearLayout.addView(maxPickerValue);

        holderLayout.addView(linearLayout);
    }

    private void addDropDownAnswerCreationToLayoutHolder(DropDownQuestion questionToDisplay) {
        //FOOTER
        ImageView addImageView = new ImageView(this.context);
        addImageView.setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.ic_add_black));
        addImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final LinearLayout linearLayout = new LinearLayout(QuestionCreationView.this.context);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.setWeightSum(100);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 90f);
                EditText answerTitle = new EditText(QuestionCreationView.this.context);
                answerTitle.setLayoutParams(lp);
                answerTitle.setHint("Intitulé de la question");

                lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 10f);
                lp.gravity = Gravity.CENTER;

                ImageView deleteAnswer = new ImageView(QuestionCreationView.this.context);
                deleteAnswer.setImageDrawable(ContextCompat.getDrawable(QuestionCreationView.this.context, R.drawable.ic_close_black));
                deleteAnswer.setLayoutParams(lp);
                deleteAnswer.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holderLayout.removeView(linearLayout);
                    }
                });
                linearLayout.addView(answerTitle);
                linearLayout.addView(deleteAnswer);
                linearLayout.setTag(new Answer());
                holderLayout.addView(linearLayout, holderLayout.getChildCount() - 1);
            }
        });
        holderLayout.addView(addImageView);

        for(Answer answer : questionToDisplay.getPossibleAnswers()) {
            createButtonOrDropDownAnswer(answer);
        }
    }

    public LinearLayout getHolderLayout() {
        return holderLayout;
    }

    public Question getQuestion() {
        switch (question.getType()) {
            case Utils.BUTTON_QUESTION_TYPE:
                if (holderLayout.getChildCount() > 2) {
                    question = createDropDownOrButtonQuestion(question, Utils.BUTTON_QUESTION_TYPE, Utils.BUTTON_ANSWER_TYPE);
                }
                break;
            case Utils.DROPDOWN_QUESTION_TYPE:
                if (holderLayout.getChildCount() > 1) {
                    question = createDropDownOrButtonQuestion(question, Utils.DROPDOWN_QUESTION_TYPE, Utils.DROPDOWN_ANSWER_TYPE);
                }
                break;
            case Utils.PICKER_QUESTION_TYPE:
                String valueMin = String.valueOf((((EditText) ((LinearLayout) holderLayout.getChildAt(0)).getChildAt(0)).getText()));
                String valueMax = String.valueOf((((EditText) ((LinearLayout) holderLayout.getChildAt(0)).getChildAt(1)).getText()));
                if (!valueMin.isEmpty() && !valueMax.isEmpty() && Integer.parseInt(valueMin) < Integer.parseInt(valueMax)) {
                    ((PickerQuestion) question).setPickerMin(Integer.parseInt(valueMin));
                    ((PickerQuestion) question).setPickerMax(Integer.parseInt(valueMax));
                }
                question.setText(String.valueOf(questionTitleEditText.getText()));
                question.setType(Utils.PICKER_QUESTION_TYPE);
                Log.d("JSON", question.getText() + " " + question.getType());
                break;
        }
        return question;
    }

    private Question createDropDownOrButtonQuestion(Question questionToSave, String questionType, String answerType) {
        ArrayList<Answer> answers = new ArrayList<>();
        int start = questionToSave.getType().equals(Utils.BUTTON_QUESTION_TYPE) ? 1 : 0;
        for (int i = start; i < holderLayout.getChildCount() - 1; i++) {
            Answer answerToCreate = (Answer) holderLayout.getChildAt(i).getTag();
            answerToCreate.setText(String.valueOf((((EditText) ((LinearLayout) holderLayout.getChildAt(i)).getChildAt(0)).getText())));
            answerToCreate.setId(new Date().toString());
            answerToCreate.setType(answerType);
            answers.add(answerToCreate);
        }
        questionToSave.setText(String.valueOf(questionTitleEditText.getText()));
        questionToSave.setType(questionType);
        questionToSave.setPossibleAnswers(answers);
        return questionToSave;
    }

    public Question createNewQuestion() {
        Question returnQuestion = getQuestion();
        setQuestion(new ButtonQuestion(new Date().toString(), "", Utils.BUTTON_QUESTION_TYPE, new ArrayList<Answer>(), 1));
        return returnQuestion;
    }
}
