package com.iem.lp.easyform.ui.view.custom;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.iem.lp.easyform.R;
import com.iem.lp.easyform.Utils;
import com.iem.lp.easyform.model.Answer;
import com.iem.lp.easyform.model.ButtonQuestion;
import com.iem.lp.easyform.model.DropDownQuestion;
import com.iem.lp.easyform.model.PickerQuestion;
import com.iem.lp.easyform.model.Question;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by aturlier on 06/01/17.
 */
public class QuestionView extends RelativeLayout implements View.OnClickListener {

    @BindView(R.id.custom_question_view_text)
    TextView questionText;

    @BindView(R.id.custom_question_view_layout_holder)
    LinearLayout holderLayout;

    private Context context = null;
    private Question question = null;
    private final int DEFAULT_ANIMATION_TRANSITION = 500;
    private int selectedAnswerCount = 0;

    public QuestionView(Context context) {
        super(context);
        init(context);
    }

    public QuestionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public QuestionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        inflate(this.context, R.layout.question_view_layout, this);
        ButterKnife.bind(this);
    }

    public void setQuestion(Question question) {
        this.fadeToInvisible(DEFAULT_ANIMATION_TRANSITION);
        this.invalidate();
        this.question = question;
        this.updateView();
        this.fadeToVisible(DEFAULT_ANIMATION_TRANSITION);
        this.invalidate();
        this.selectedAnswerCount = 0;
    }

    private void fadeToInvisible(int duration) {
        AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration(duration);
        animation.setFillAfter(true);
        this.startAnimation(animation);
    }

    private void fadeToVisible(int duration) {
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(duration);
        animation.setFillAfter(true);
        this.startAnimation(animation);
    }

    private void updateView() {
        this.questionText.setText(question.getText());
        this.cleanHolderLayout();
        addAnswersToLayoutHolder(question);
    }

    private void cleanHolderLayout() {
        holderLayout.removeAllViews();
    }

    private void addAnswersToLayoutHolder(Question questionToDisplay) {
        switch(question.getType()) {
            case Utils.BUTTON_QUESTION_TYPE :
                addButtonsToLayoutHolder((ButtonQuestion) questionToDisplay);
                break;
            case Utils.PICKER_QUESTION_TYPE :
                addPickerToLayoutHolder((PickerQuestion) questionToDisplay);
                break;
            case Utils.DROPDOWN_QUESTION_TYPE :
                addDropDownToLayoutHolder((DropDownQuestion) questionToDisplay);
                break;
            default:
                break;
        }
    }

    private void addPickerToLayoutHolder(final PickerQuestion questionToDisplay) {
        NumberPicker picker = new NumberPicker(this.context);
        picker.setMinValue(questionToDisplay.getPickerMin());
        picker.setMaxValue(questionToDisplay.getPickerMax());
        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                Intent intent = new Intent();
                intent.setAction("com.iem.lp.easyform.SENDQUESTION");
                questionToDisplay.getAnswer().setText(Integer.toString(newVal));
                intent.putExtra("ANSWER", questionToDisplay.getAnswer());
                context.sendBroadcast(intent);
            }
        });
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1f);
        picker.setLayoutParams(lp);
        picker.setValue(Integer.parseInt(questionToDisplay.getAnswer().getText()));
        holderLayout.addView(picker);
    }

    private void addDropDownToLayoutHolder(final DropDownQuestion questionToDisplay) {
        Spinner spinner = new Spinner(this.context);
        ArrayList<String> answerText = new ArrayList<>();
        for (Answer answer : questionToDisplay.getPossibleAnswers()) {
            answerText.add(answer.getText());
        }
        ArrayAdapter<String> possibleAnswersArrayAdapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_dropdown_item, answerText);
        spinner.setAdapter(possibleAnswersArrayAdapter);
        int px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                px,
                1f);
        spinner.setLayoutParams(lp);
        holderLayout.addView(spinner);
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("DEBUG SPINNER", Integer.toString(position));
                Intent intent = new Intent();
                intent.setAction("com.iem.lp.easyform.SENDQUESTION");
                intent.putExtra("ANSWER", questionToDisplay.getPossibleAnswers().get(position));
                context.sendBroadcast(intent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d("DEBUG SPINNER", "Nothing");
                Intent intent = new Intent();
                intent.setAction("com.iem.lp.easyform.SENDQUESTION");
                intent.putExtra("ANSWER", questionToDisplay.getPossibleAnswers().get(0));
                context.sendBroadcast(intent);
            }
        });
    }

    private void addButtonsToLayoutHolder(ButtonQuestion questionToDisplay) {
        for (Answer ans : question.getPossibleAnswers()) {
            if (ans.isSelected()) {
                selectedAnswerCount ++;
            }
            addButtonToLayoutHolder(ans);
        }
    }

    private void addButtonToLayoutHolder(Answer answer) {
        Button myButton = new Button(getContext());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            myButton.setBreakStrategy(Layout.BREAK_STRATEGY_BALANCED);
        }
        myButton.setText(answer.getText());
        myButton.setTag(answer);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1f);
        int px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
        lp.setMargins(px, 0, px, 0);
        myButton.setLayoutParams(lp);
        myButton.setOnClickListener(this);
        myButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ripple_green));
        if(answer.isSelected()) {
            myButton.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.buttonSelected));
        } else {
            myButton.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        }
        holderLayout.addView(myButton);
    }

    private void resetButtonsBackground() {
        for (int i = 0; i < holderLayout.getChildCount(); i++) {
            holderLayout.getChildAt(i).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        }
    }

    @Override
    public void onClick(View view) {
        if(question.getType().equals(Utils.BUTTON_QUESTION_TYPE)) {
            if (!view.isSelected()) {
                if (selectedAnswerCount < ((ButtonQuestion)question).getMaxNumberOfChoices()) {
                    selectedAnswerCount++;
                    view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.buttonSelected));
                    view.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ripple_green));
                    view.setSelected(true);
                    Intent intent = new Intent();
                    intent.setAction("com.iem.lp.easyform.SENDQUESTION");
                    intent.putExtra("ANSWER", (Answer) view.getTag());
                    context.sendBroadcast(intent);
                }
            } else {
                view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                view.setSelected(false);
                selectedAnswerCount--;
                Intent intent = new Intent();
                intent.setAction("com.iem.lp.easyform.SENDQUESTION");
                intent.putExtra("ANSWER", (Answer) view.getTag());
                context.sendBroadcast(intent);
            }
        }
    }
}
